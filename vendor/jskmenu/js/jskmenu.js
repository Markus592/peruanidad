var jskMenuScroll = 0;

$( () => {
	
	//On Scroll Functionality
	
	

	jQuery(window).scroll( () => {
		var windowTop = jQuery(window).scrollTop();
		//console.log(windowTop,"windowTOP");
		windowTop > 150 ? jQuery('#header').addClass('scrolled') : jQuery('#header').removeClass('scrolled');
		//windowTop > 150 ? $('#mainMenu').css('top','0px') : $('ul').css('top','160px');
	});
	
	//Click Logo To Scroll To Top
	$('#logo').on('click', () => {
		$('html,body').animate({scrollTop: 0},300);
	});
	
	//Smooth Scrolling Using Navigation Menu
	$('a[href*="#"]').on('click', function(e){
		var windowWidth = $(window).width();
		jskMenuScroll = 10;
		if(windowWidth<1000){
			jskMenuScroll = 0;
		}
		$('html,body').animate({
			scrollTop: $($(this).attr('href')).offset().top - MenuScroll},200);
		e.preventDefault();
		toggleNav();

	});
	
	//Toggle Menu
	$('#menu-toggle').on('click', () => {
		$('#menu-toggle').toggleClass('closeMenu');
		$('ul').toggleClass('showMenu');
		
		$('li').on('click', () => {
			$('ul').removeClass('showMenu');
			$('#menu-toggle').removeClass('closeMenu');
		});
	});
    
    
    $('.hamburger').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav();
    });  

});

function toggleNav() {
	var windowWidth = $(window).width();
	//console.log(windowWidth,"WINDOWS WIDTH");
	if(windowWidth <1000){
		jQuery(".hamburger").toggleClass("is-active");
		jQuery("#mainWrapper").toggleClass("show-nav");
		jQuery(".jskResponsiveMenu").toggleClass("responsive");
	}


}